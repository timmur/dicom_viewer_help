#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <string>
#include <ctime>
using namespace std;
typedef unsigned char byte;

struct Pixel {
	int x; // coords
	int y; // ����� �� �������������� � �� ������������� � ����������, �.�. ����� ��������� � ���������� �������
	int value; // HU
	bool used; // ��� ������ � ������
};

// ���� �� ��������� ����� - 2������ ������
class FileInfo {
public:
	Pixel*** mas; // the image
	int*** aNormalizedPixelBuffer;
	int rowCount; // height / width
	int colCount;
	int sliceCount;


	void Normalize(int c, int w)
	{
		int aWindowLeftBorder = c - (w / 2);

		// Normalize the Pixel value to [0,255]
		this->aNormalizedPixelBuffer = new int**[this->rowCount];
		for (int i = 0; i < this->rowCount; i++)
		{
			aNormalizedPixelBuffer[i] = new int*[this->colCount];

			for (int j = 0; j < this->colCount; j++)
				aNormalizedPixelBuffer[i][j] = new int[this->sliceCount];
		}

		for (int n = 0; n < sliceCount; n++)
		{
			for (int aRowIndex = 0; aRowIndex < rowCount; aRowIndex++)
			{
				for (int aColumnIndex = 0; aColumnIndex < colCount; aColumnIndex++)
				{
					int aPixelValueNormalized = (255 * (mas[aRowIndex][aColumnIndex][n].value - aWindowLeftBorder)) / w;

					if (aPixelValueNormalized <= 0)
						aNormalizedPixelBuffer[aRowIndex][aColumnIndex][n] = 0;
					else
						if (aPixelValueNormalized >= 255)
							aNormalizedPixelBuffer[aRowIndex][aColumnIndex][n] = 255;
						else
							aNormalizedPixelBuffer[aRowIndex][aColumnIndex][n] = aPixelValueNormalized;
				}
			}
		}
		// Allocate the Pixel Array for the Bitmap (4 Byte: R, G, B, Alpha value)
		//aImageDataArray = new byte*[sliceCount];


		//int i = 0;
		//for (int n = 0; n < sliceCount; n++)
		//{
		//	aImageDataArray[n] = new byte[rowCount * colCount];
		//}
		//for (int n = 0; n < sliceCount; n++)
		//{
		//	for (int aRowIndex = 0; aRowIndex < rowCount; aRowIndex++)
		//	{
		//		for (int aColumnIndex = 0; aColumnIndex < colCount; aColumnIndex++)
		//		{
		//			byte aGrayValue = (byte)aNormalizedPixelBuffer[aRowIndex][aColumnIndex][n];

		//			// Black/White image: all RGB values are set to same value
		//			aImageDataArray[n][i] = aGrayValue;

		//			i++;
		//		}
		//	}
		//}
	}

	FileInfo() 
	{
		string name = "..\\data\\buffer.dat";
		ifstream br(name.c_str(), ios::in | ios::binary);
		if (br.good()) {
			cout << "found file!" << endl;
			br.read((char*)&this->rowCount, sizeof(int));

			br.read((char*)&this->colCount, sizeof(int));

			br.read((char*)&this->sliceCount, sizeof(int));

			this->mas = new Pixel**[this->rowCount];
			for (int i = 0; i < this->rowCount; i++)
			{
				this->mas[i] = new Pixel*[this->colCount];

				for (int j = 0; j < this->colCount; j++)
					this->mas[i][j] = new Pixel[this->sliceCount];
			}
			for (int n = 0; n < this->sliceCount; n++)
				for (int i = 0; i < this->rowCount; i++)
					for (int j = 0; j < this->colCount; j++) {
						br.read((char*)&this->mas[i][j][n].value, sizeof(int));
						this->mas[i][j][n].x = i;
						this->mas[i][j][n].y = j;
						//this->mas[i][j][n].value < 3000 ? this->mas[i][j][n].used = false : this->mas[i][j][n].used = true;
					}

			br.close();
		}
		else {
			cout << "no such file!" << endl;
			br.close();
		}
	}
	~FileInfo() {}
};


void main()
{
	short wc, ww;
	cout << "Window center : "; cin >> wc;
	cout << "Window width : "; cin >> ww;
	int t1 = clock();
	FileInfo CTSliceInfo = FileInfo();
	CTSliceInfo.Normalize(wc, ww); // bones
	byte header[14] = {
		'B','M', // black magic
		0,0,0,0, // size in bytes
		0,0, // app data
		0,0, // app data
		40 + 14,0,0,0 // start of data offset
	};
	byte info[40] = {
		40,0,0,0, // info hd size
		0,0,0,0, // width
		0,0,0,0, // heigth
		1,0, // number color planes
		24,0, // bits per pixel
		0,0,0,0, // compression is none
		0,0,0,0, // image bits size
		0x13,0x0B,0,0, // horz resoluition in pixel / m
		0x13,0x0B,0,0, // vert resolutions (0x03C3 = 96 dpi, 0x0B13 = 72 dpi)
		0,0,0,0, // #colors in pallete
		0,0,0,0, // #important colors
	};

	int w = CTSliceInfo.rowCount;
	int h = CTSliceInfo.colCount;

	int padSize = (4 - (w * 3) % 4) % 4;
	int sizeData = w*h * 3 + h*padSize;
	int sizeAll = sizeData + sizeof(header) + sizeof(info);

	header[2] = (byte)(sizeAll);
	header[3] = (byte)(sizeAll >> 8);
	header[4] = (byte)(sizeAll >> 16);
	header[5] = (byte)(sizeAll >> 24);

	info[4] = (byte)(w);
	info[5] = (byte)(w >> 8);
	info[6] = (byte)(w >> 16);
	info[7] = (byte)(w >> 24);

	info[8] = (byte)(h);
	info[9] = (byte)(h >> 8);
	info[10] = (byte)(h >> 16);
	info[11] = (byte)(h >> 24);

	info[20] = (byte)(sizeData);
	info[21] = (byte)(sizeData >> 8);
	info[22] = (byte)(sizeData >> 16);
	info[23] = (byte)(sizeData >> 24);



	for (int n = 0; n < CTSliceInfo.sliceCount; n++)
	{
		string name = "..\\output\\";
		stringstream nm;
		nm << name << n << ".bmp";
		name = ""; nm >> name;
		ofstream bw(name.c_str(), ios::out | ios::binary);
		if (bw.good())
		{

			bw.write((char*)header, sizeof(header));
			bw.write((char*)info, sizeof(info));

			byte pad[3] = { 0,0,0 };

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					byte pixel[3];
					pixel[0] = pixel[1] = pixel[2] = (byte) CTSliceInfo.aNormalizedPixelBuffer[x][y][n];

					bw.write((char*)pixel, 3);
				}
				bw.write((char*)pad, padSize);
			}
		}
	}
	cout << (double)(clock() - t1) / CLOCKS_PER_SEC << "sec. Done!" << endl;
	system("pause");
}
